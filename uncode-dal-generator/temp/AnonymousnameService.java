package cn.uncode.dal.service;

import cn.uncode.dal.dto.Anonymousname;

import cn.uncode.dal.service.BaseService;
 /**
 * service接口类,此类由Uncode自动生成
 * @author uncode
 * @date 2017-02-21
 */
public interface AnonymousnameService extends BaseService<Anonymousname> {

}